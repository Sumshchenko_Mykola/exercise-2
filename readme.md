mkdir pro
cd pro
touch readme.md
git add .
git commit -m "repo: initial commit"
mkdir ex02
cd ex02
git init
git checkout -b first_branch
gedit readme.md 
git status
git commit -am "readme: add the command log of the 1st subtask"
git checkout master
gedit readme.md
git commit -am "readme: add command log to solve 2nd subtask"
git log --oneline --decorate --graph --all